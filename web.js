const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const fs = require('fs');
const NILOD = require('.');
const bodyParser = require('body-parser');

let historic = [];

app.get('/', (req, res) => {
    console.log(`[${req.headers['x-forwarded-for'] || req.connection.remoteAddress}] New connection.`);
    res.write(fs.readFileSync('web/index.html', 'utf-8'));
    res.end();
});

let sentenceCallbacks = [];

function addedSentence(inp, sent, id) {
    historic.push({
        input: inp,
        sent: sent,
        id: id
    });

    if (historic.length >= 20)
        historic.shift();

    sentenceCallbacks.forEach((cb) => cb(inp, sent, id));
}

io.on('connection', (socket) => {
    socket.on('getHistoric', () => {
        socket.emit('historic', historic);
    });

    sentenceCallbacks.push((input, sentence, id) => {
        socket.emit('rateme', {
            input: input,
            sentence: sentence,
            output: sentence,
            attemptID: id,
            best: {
                output: sentence,
                id: id,
                attempt: {
                    input: input,
                    output: sentence,
                    attemptID: id
                }
            }
        });
    });
});

app.use('/', express.static('web'));

app.use(bodyParser.json());

app.post('/api/nilod/ratesentence', (req, res) => {
    let model = NILOD(fs.readFileSync('web/current.nilod.json', 'utf-8'));

    res.json({ newRating: model.applyRating(req.body.id, req.body.rating) });
    res.end();

    io.emit('rated', {
        id: req.body.id,
        rate: req.body.rating
    });
});

app.post('/api/nilod/savesentence', (req, res) => {
    res.end();

    let model = NILOD(fs.readFileSync('web/current.nilod.json', 'utf-8'));

    model.outputHistory[req.body.sentence.attemptID] = {
        output: req.body.sentence,
        rating: 0,
        ratingCount: 0
    };

    fs.writeFileSync('web/current.nilod.json', model.save());
    addedSentence(null, req.body.sentence.output, req.body.sentence.attemptID);
});

app.post('/api/nilod/savemodel', (req, res) => {
    res.end();

    let model = NILOD(fs.readFileSync('web/current.nilod.json', 'utf-8'));
    model.train(req.body.sentence);

    if (req.body.answer) {
        model.outputHistory[req.body.answer.attemptID] = {
            output: req.body.answer,
            rating: 0,
            ratingCount: 0
        };
    }

    fs.writeFileSync('web/current.nilod.json', model.save());
    addedSentence(req.body.sentence, req.body.answer.output, req.body.answer.attemptID);

    console.log(`[${req.headers['x-forwarded-for'] || req.connection.remoteAddress}] asked: "${req.body.sentence}" -> ${(req.body.answer && req.body.answer.output) ? `"${req.body.answer.output}"` : '~~'}`);
});

let listenPort = process.argv[2] != null && !isNaN(process.argv[2]) ? parseInt(process.argv[2]) : 8555;

server.listen(listenPort, () => {
    console.log('Listening on port:', listenPort);
});